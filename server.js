// ------------------------------------------------------- LIBRAIRIES
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const { cnxArlenShake, cnxWildMagasin } = require('./conf');
const mysql = require('mysql');

const PORT_SERVER = 8080;

// --------------------------------------------------------- MIDDLEWARES

// Etre capable de lire les paramètres ajoutés à la fin de ma route
app.use(bodyParser.urlencoded({ extended: false }));
// Etre capable de lire les données envoyées en JSON *en plus* de ma route
app.use(bodyParser.json());
// Pouvoir récupérer la réponse du serveur dans le navigateur de mon client
app.use(cors());

// ----------------------------------------------------------------- ROUTES
// GET / : Laisser un message
app.get('/', (req, res) => {
  console.log(`Called GET '/' : I'll just leave a message`);

  res.status(200).send('Hello World !\n');
});

// GET /users : Liste d'utilisateurs
app.get('/users', (req, res) => {
  console.log(`Called GET '/users' : I'll list my users`);

  // On demande l'exécution d'une requête, et on exécute la fonctin de callback dès que le
  // serveur de BdD aura répondu (avec des résultats et/ou une erreur)
  cnxArlenShake.query('SELECT * FROM users', (err, results) => {
    if (err) {
      res.status(500).send(`We crashed, here is the message : ${err}`);
    } else {
      res.status(200).json(results);
    }
  });
});

// GET /users/ID : Infos de l'utilisateur n°ID
app.get('/users/:id', (req, res) => {
  // On commence par stocker le paramètre id (ce qui remplace ":id" dans la route appelée)
  let id = req.params.id;
  console.log(`Called GET '/users/${id}' : I'll fetch data about him/her`);
  // On échappe "id" afin que le vsiiteur ne puisse pas mettre n'importe quoi.
  // On n'aura alors rien qui pourrait être *exécuté* sur le serveur
  id = mysql.escape(id);

  cnxArlenShake.query(
    `SELECT * FROM users WHERE id='${id}'`,
    (err, results) => {
      if (err) {
        res.status(500).send(`We crashed, here is the message : ${err}`);
      } else {
        res.status(200).json(results);
      }
    },
  );
});

// GET /users/ID : Infos de l'utilisateur n°ID
app.get('/products', (req, res) => {
  console.log(`Called GET '/products' : I'll fetch data about products`);
  // On échappe "id" afin que le vsiiteur ne puisse pas mettre n'importe quoi.
  // On n'aura alors rien qui pourrait être *exécuté* sur le serveur

  cnxWildMagasin.query(
    `SELECT rayon.nom AS rayon, SUM(ROUND(prix_unitaire_HT*(1+taux_TVA/100),2)) AS prix_TTC FROM produit INNER JOIN rayon ON produit.id_rayon = rayon.id GROUP BY rayon ORDER BY rayon DESC`,
    (err, results) => {
      if (err) {
        res.status(500).send(`We crashed, here is the message : ${err}`);
      } else {
        res.status(200).json(results);
      }
    },
  );
});

// ------------------------------------------------ 404 + LANCEMENT SERVEUR
app.listen(PORT_SERVER, () => {
  console.log(`Listening on port ${PORT_SERVER}...`);
});
