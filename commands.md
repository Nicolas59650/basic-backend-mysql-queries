## Initialisation

mkdir arlen-shake ; cd arlen-shake
npm init -y
npm install --save express cors body-parser mysql
touch server.js
touch conf.js
cp conf.js dist.conf.js
touch .gitignore

## Installation & Démarrage

cd arlen-shake
npm install
cp dist.conf.js conf.js
// Ajouter vrais ID de connexion dans le conf.js
nodemon server.js

## Utilisation

curl http://localhost:8080/
curl http://localhost:8080/users/4
curl http://localhost:8080/users?email=toto@gmail.com

curl -X POST http://localhost:8080/login -H 'Content-Type: application/json' -d '{"email":"test@test.com", "password":"password"}'
